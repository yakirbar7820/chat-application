const database = require("../../database/models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = database.user;

module.exports = {
    signIn: async (request, response) => {
        const { username, password } = request.body;

        if (!username || !password) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😕 Both username and password are required. Please provide them." })
            );
        };

        const existingUser = await User.findOne({ username });

        if (!existingUser) {
            return (
                response
                    .status(404)
                    .json({ message: "Oops! 😢 Username not found. Please sign up to create an account." })
            );
        };

        bcrypt.compare(password, existingUser.password, (err, same) => {
            if (err) {
                return (
                    response
                        .status(500)
                        .json({ message: "Oops! 😱 Something went wrong while comparing passwords. Please try again later." })
                );
            };

            if (!same) {
                return (
                    response
                        .status(401)
                        .json({ message: "Oops! 😠 Incorrect password. Please try again." })
                );
            };

            const token = jwt.sign({ senderId: existingUser._id }, process.env.JWT_SECRET, { expiresIn: "15d" });

            return (
                response
                    .status(200)
                    .json({
                        token,
                        message: "🎉 Welcome back! You have successfully signed in."
                    })
            );
        });
    },
    signUp: async (request, response) => {
        const { username } = request.body;

        if (!username) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😕 Username is missing. Please provide a username." })
            );
        };

        const existingUser = await User.findOne({ username });

        if (existingUser) {
            return (
                response
                    .status(409)
                    .json({ message: "Oops! 😠 This username is already taken. Please choose a different one." })
            );
        };

        await new User({ username, password: request.encrypted }).save()
            .then((user) => {
                return (
                    response
                        .status(201)
                        .json({
                            user,
                            message: "🎉 Congratulations! You have successfully signed up and created an account."
                        })
                );
            })
            .catch((err) => {
                return (
                    response
                        .status(500)
                        .json({
                            error: err.message,
                            message: "Oops! 😱 Something went wrong while signing up. Please try again later."
                        })
                );
            });
    },
    signOut: async (request, response) => {
        const { senderId } = request.decoded;

        return (
            response
                .status(200)
                .json({
                    message: "👋 You have been successfully signed out. Goodbye!"
                })
        );
    }
};