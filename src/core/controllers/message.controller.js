const database = require("../../database/models");
const Conversation = database.conversation;
const mongoose = require("mongoose");
const Message = database.message;
const User = database.user;

module.exports = {
    getMessage: async (request, response) => {
        const { senderId } = request.decoded;
        const { id: receiverId } = request.params;

        if (!senderId || !receiverId) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😕 Both senderId and receiverId are required. Please provide them." })
            );
        };

        if (!mongoose.Types.ObjectId.isValid(receiverId)) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😠 Invalid receiverId provided. It must be a valid MongoDB ObjectID." })
            );
        };

        const existingSender = await User.findById(senderId);
        const existingReceiver = await User.findById(receiverId);

        if (!existingSender || !existingReceiver) {
            return (
                response
                    .status(404)
                    .json({ message: "Oops! 😢 Sender or receiver not found. Please check the provided IDs." })
            );
        };

        const conversation = await Conversation.findOne({ participants: { $all: [senderId, receiverId] } });

        return (
            response
                .status(200)
                .json({
                    conversation: conversation ? conversation.messages : [],
                    message: "📬 Messages retrieved successfully!"
                })
        );
    },
    setMessage: async (request, response) => {
        const { content } = request.body;
        const { senderId } = request.decoded;
        const { id: receiverId } = request.params;

        if (!senderId || !receiverId || !content) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😕 senderId, receiverId and content are all required. Please provide them." })
            );
        };

        if (!mongoose.Types.ObjectId.isValid(receiverId)) {
            return (
                response
                    .status(400)
                    .json({ message: "Oops! 😠 Invalid receiverId provided. It must be a valid MongoDB ObjectID." })
            );
        };

        const existingSender = await User.findById(senderId);
        const existingReceiver = await User.findById(receiverId);

        if (!existingSender || !existingReceiver) {
            return (
                response
                    .status(404)
                    .json({ message: "Oops! 😢 Sender or receiver not found. Please check the provided IDs." })
            );
        };

        const conversation = await Conversation.findOne({ participants: { $all: [senderId, receiverId] } }) || await Conversation.create({ participants: [senderId, receiverId] });

        await new Message({ content, senderId, receiverId }).save()
            .then(async (message) => {
                conversation.messages.push(message._id);

                await conversation.save();

                return (
                    response
                        .status(201)
                        .json({
                            content: message,
                            message: "✉️ Message sent successfully!"
                        })
                );
            })
            .catch((err) => {
                return (
                    response
                        .status(500)
                        .json({
                            error: err.message,
                            message: "Oops! 😱 Something went wrong while adding the message. Please try again later."
                        })
                );
            });
    }
};