const jwt = require("jsonwebtoken");

module.exports = (request, response, next) => {
    const authHeader = request.headers["authorization"];

    if (!authHeader) {
        return (
            response
                .status(400)
                .json({ message: "Oops! 😕 Authorization header is missing. Please include it in your request." })
        );
    };

    const token = authHeader.split(" ")[1];

    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
        if (err) {
            if (err.name === "TokenExpiredError") {
                return (
                    response
                        .status(401)
                        .json({
                            error: err.message,
                            message: "Oops! 😞 Your access token has expired. Please log in again to get a fresh one."
                        })
                );
            } else if (err.name === "JsonWebTokenError") {
                return (
                    response
                        .status(401)
                        .json({
                            error: err.message,
                            message: "Oops! 😠 Your access token seems to be invalid. Please make sure you're using the correct token."
                        })
                );
            } else if (err.name === "NotBeforeError") {
                return (
                    response
                        .status(401)
                        .json({
                            error: err.message,
                            message: "Oops! 😢 It looks like your access token is not yet valid. Please wait for the token to become active."
                        })
                );
            } else {
                return (
                    response
                        .status(500)
                        .json({
                            error: err.message,
                            message: "Oops! 😱 Something went wrong while verifying your access token. Please try again later."
                        })
                );
            };
        };

        if (!decoded) {
            return (
                response
                    .status(403)
                    .json({ message: "Oops! 😓 You don't have permission to access this resource." })
            );
        };

        request.decoded = decoded;
        next();
    });
};