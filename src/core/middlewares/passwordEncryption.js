const bcrypt = require("bcrypt");

module.exports = (request, response, next) => {
    const { password, verifyPassword } = request.body;

    if (!password || !verifyPassword) {
        return (
            response
                .status(400)
                .json({ message: "Oops! 😕 Both password and verify password are required. Please provide them." })
        );
    };

    if (password !== verifyPassword) {
        return (
            response
                .status(400)
                .json({ message: "Oops! 😠 Passwords do not match. Please make sure they are the same." })
        );
    };

    bcrypt.genSalt(10, (err, salt) => {
        if (err) {
            return (
                response
                    .status(500)
                    .json({
                        error: err.message,
                        message: "Oops! 😱 Something went wrong while generating salt. Please try again later."
                    })
            );
        };

        if (!salt) {
            return (
                response
                    .status(500)
                    .json({ message: "Oops! 😢 Salt was not generated. Please try again later." })
            );
        };

        bcrypt.hash(password, salt, (err, encrypted) => {
            if (err) {
                return (
                    response
                        .status(500)
                        .json({
                            error: err.message,
                            message: "Oops! 😞 Something went wrong while encrypting the password. Please try again later."
                        })
                );
            };

            if (!encrypted) {
                return (
                    response
                        .status(500)
                        .json({ message: "Oops! 😓 Encryption failed. Please try again later." })
                );
            };

            request.encrypted = encrypted;
            next();
        });
    });
};