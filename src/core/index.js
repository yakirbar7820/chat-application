const express = require("express");
const signRoutes = require("./routes/sign.routes");
const messageRoutes = require("./routes/message.routes");

const app = express();

app.use(express.json());

app.use(signRoutes);
app.use(messageRoutes);

module.exports = {
    app, // Exporting express app instance 🚀
    httpPort: process.env.HTTP_PORT || 8000, // HTTP port (default: 8000) 🌐
    httpsPort: process.env.HTTPS_PORT || 8443 // HTTPS port (default: 8443) 🔒
};