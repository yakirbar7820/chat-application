const express = require("express");

const router = express.Router();

const {
    signIn,
    signUp,
    signOut
} = require("../controllers/sign.controller");

const authenticateToken = require("../middlewares/authenticateToken");

const passwordEncryption = require("../middlewares/passwordEncryption");

router.post("/sign-in", signIn);
router.post("/sign-up", passwordEncryption, signUp);
router.post("/sign-out", authenticateToken, signOut);

module.exports = router;