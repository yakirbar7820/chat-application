const express = require("express");

const router = express.Router();

const {
    getMessage,
    setMessage
} = require("../controllers/message.controller");

const authenticateToken = require("../middlewares/authenticateToken");

router.get("/get-message/:id", authenticateToken, getMessage);
router.post("/set-message/:id", authenticateToken, setMessage);

module.exports = router;