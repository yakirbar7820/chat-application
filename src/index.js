require("dotenv").config(); // Loading environment variables from .env file ⚙️

const http = require("http"); // Importing http module for creating HTTP server 🌐

const https = require("https"); // Importing https module for creating HTTPS server 🔒

const SSL_certificates = require("../private"); // Importing SSL certificates for HTTPS server 📜

const connectToMongoDB = require("./database"); // Importing function to connect to MongoDB from database module 📂

const { app, httpPort, httpsPort } = require("./core"); // Importing app instance and port numbers from core module 🔌

http
    .createServer(app) // Creating HTTP server with the imported app 🚀
    .listen(httpPort, () => {
        connectToMongoDB(); // Connecting to MongoDB 🔄
        console.log(`HTTP server running on port ${httpPort}`); // Logging HTTP server port ℹ️
    });

https
    .createServer(SSL_certificates, app) // Creating HTTPS server with SSL certificates and the imported app 🔒
    .listen(httpsPort, () => {
        connectToMongoDB(); // Connecting to MongoDB 🔄
        console.log(`HTTPS server running on port ${httpsPort}`); // Logging HTTPS server port ℹ️
    });