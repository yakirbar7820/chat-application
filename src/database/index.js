const mongoose = require("mongoose");

const { handleMongoDBError } = require("./errors");

module.exports = async () => {
    await mongoose.connect(process.env.URI)
        .then(() => {
            console.log("Connected to MongoDB successfully! 🚀");
        })
        .catch(handleMongoDBError);
};