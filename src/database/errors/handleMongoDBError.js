const mongoose = require("mongoose"); // 🍃 Importing mongoose for MongoDB object modeling tool

module.exports = (err) => {
    if (err instanceof mongoose.Error) { // 🚨 Checking if the error is an instance of a mongoose error
        require("./functions").handleMongooseError(err); // 🔍 Handling mongoose-specific errors
    } 
    else if (err.name === "MongoError") { // 🚨 Checking if the error name is "MongoError"
        require("./functions").handleMongoError(err); // 🔍 Handling MongoDB errors
    } 
    else {
        require("./functions").handleGeneralError(err); // 🔍 Handling general errors
    };
};