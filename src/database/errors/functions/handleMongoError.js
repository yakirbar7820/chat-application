module.exports = (err) => {
    switch (err.code) {
        case 11000:
        case 11001:
            console.error("😫 Oops! It looks like there's a duplicate key error. The provided data already exists in the database.");
            break; // Duplicate key error handling
        case 2:
            console.error("😩 Oops! It seems like you missed a required field. Please provide all necessary information.");
            break; // Missing required field error handling
        case 18:
            console.error("😔 Oops! Authentication failed. Please check your MongoDB credentials and try again.");
            break; // Authentication error handling
        case 13:
            console.error("😢 Uh-oh! It seems like you don't have the necessary permissions to perform this operation. Please contact your administrator.");
            break; // Permission error handling
        case 9:
            console.error("😨 Oops! We couldn't parse the server URL. Please review your MongoDB connection string and try again.");
            break; // Server URL parsing error handling
        case 59:
            console.error("😰 Oops! We couldn't find any suitable servers. Please review your MongoDB server configuration.");
            break; // No suitable servers error handling
        case 61:
            console.error("😡 Uh-oh! It seems like we received an unrecognized server response. Please verify your MongoDB server status and try again.");
            break; // Unrecognized server response error handling
        case 66:
            console.error("😣 Oops! It looks like you've exceeded the storage quota. Please free up some space and try again.");
            break; // Exceeded storage quota error handling
        case 67:
            console.error("😩 Uh-oh! It seems like there's an issue with the client metadata. Please verify the client metadata sent to the server and try again.");
            break; // Client metadata error handling
        case 50:
            console.error("😖 Oops! It looks like you've exceeded the maximum connection pool size. Please review your application's connection usage.");
            break; // Exceeded maximum connection pool size error handling
        case 68:
            console.error("😓 Uh-oh! It seems like there's a server metadata error. Please verify the server metadata received from the server and try again.");
            break; // Server metadata error handling
        case 72:
            console.error("😰 Oops! It looks like there's a legacy mode error. Please verify your MongoDB server compatibility.");
            break; // Legacy mode error handling
        case 85:
            console.error("😧 Uh-oh! It seems like there's a TLS configuration error. Please verify your TLS configuration and try again.");
            break; // TLS configuration error handling
        default:
            console.error("😞 Uh-oh! Something went wrong with the database:", err.message);
            break; // Generic error handling
    };
};