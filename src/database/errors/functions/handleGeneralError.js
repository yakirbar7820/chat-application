module.exports = (err) => {
    console.error("😱 Oops! Something went wrong while connecting to the database:", err.message); // 🔍 Log error message when connecting to the database fails
};