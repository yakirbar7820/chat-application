module.exports = {
    handleMongoError: require("./handleMongoError"), // 🚀 Exporting handleMongoError function for MongoDB-specific errors
    handleGeneralError: require("./handleGeneralError"), // 🚀 Exporting handleGeneralError function for general errors
    handleMongooseError: require("./handleMongooseError") // 🚀 Exporting handleMongooseError function for Mongoose errors
};