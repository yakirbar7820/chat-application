module.exports = (err) => {
    switch (err.name) {
        case "MongoTimeoutError":
            console.error("😟 Oops! We couldn't connect to the database. It seems like there's a timeout issue. Please check your internet connection and try again.");
            break; // Connection timeout error handling
        case "MongoNetworkError":
            console.error("😞 Uh-oh! It looks like we can't connect to the database. Please ensure your MongoDB server is up and running, and your network connection is stable.");
            break; // Network error handling
        case "MongooseServerSelectionError":
            console.error("😓 We're having trouble selecting the MongoDB server. Make sure your MongoDB server is running and accessible.");
            break; // Server selection error handling
        case "ValidationError":
            console.error("😔 Oops! We encountered some validation errors while saving data. Please review your input and try again.");
            if (err.errors) {
                Object.keys(err.errors).forEach((field) => {
                    console.error(`\t${field}: ${err.errors[field].message}`);
                });
            };
            break; // Validation error handling
        case "CastError":
            console.error("😣 Uh-oh! It seems like you've provided invalid data. Please make sure you're using the correct data type.");
            break; // Cast error handling
        default:
            console.error("😕 Oops! Something went wrong with the database:", err.message);
            break; // Generic error handling
    };
};