const database = {};

const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

database.mongoose = mongoose;

database.user = require("./User");

database.message = require("./Message");

database.conversation = require("./Conversation");

module.exports = database;