const mongoose = require("mongoose");

const messageSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    senderId: {
        ref: "User",
        required: true,
        type: mongoose.Schema.Types.ObjectId
    },
    receiverId: {
        ref: "User",
        required: true,
        type: mongoose.Schema.Types.ObjectId
    }
}, { versionKey: false, timestamps: true });

module.exports = mongoose.model("Message", messageSchema);