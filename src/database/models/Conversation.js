const mongoose = require("mongoose");

const conversationSchema = new mongoose.Schema({
    participants: [{
        ref: "User",
        required: true,
        type: mongoose.Schema.Types.ObjectId
    }],
    messages: [{
        ref: "Message",
        required: true,
        type: mongoose.Schema.Types.ObjectId
    }]
}, { versionKey: false, timestamps: true });

module.exports = mongoose.model("Conversation", conversationSchema);